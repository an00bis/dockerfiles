based on this guide: https://discourse.pi-hole.net/t/dhcp-with-docker-compose-and-bridge-networking/17038
# steps:
* adjust docker-compose.yml to personal liking
* create docker network: `docker network create frontproxy_proxy-tier`
* create file  /opt/pihole/etc-dnsmasq.d/07-dhcp-options (container volume location) with the following content:
  * `dhcp-option=option:dns-server, <pi IP>`
* docker-compose up 

# notes:
* had to specify the alpine version to 3.10 since the latest release did not work. not clear why.
